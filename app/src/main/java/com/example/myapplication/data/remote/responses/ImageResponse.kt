package com.example.myapplication.data.remote.responses

class ImageResponse(
    val hits: List<ImageResult>,
    val total: Int,
    val totalHits: Int
)