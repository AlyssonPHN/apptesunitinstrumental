package com.example.myapplication.repositories

import androidx.lifecycle.LiveData
import com.example.myapplication.data.local.ShoppingItem
import com.example.myapplication.other.Resource
import retrofit2.Response
import com.example.myapplication.data.remote.responses.ImageResponse as ImageResponse

interface ShoppingRepository {

    suspend fun insertShoppingItem(shoppingItem: ShoppingItem)

    suspend fun deleteShoppingItem(shoppingItem: ShoppingItem)

    fun observeAllShoppingItems(): LiveData<List<ShoppingItem>>

    fun observeTotalPrice(): LiveData<Float>

    suspend fun searchForImage(imageQuery: String): Resource<ImageResponse>
}