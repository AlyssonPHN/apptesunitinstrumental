package com.example.myapplication

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.example.myapplication.ui.fimTeste.ResourceComparer
import com.google.common.truth.Truth.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test

class ResourceComparerTest {
    private var resourceComparer: ResourceComparer? = null

    @Before
    fun setup() {
        resourceComparer = ResourceComparer()
    }

    @After
    fun teardown() {
        resourceComparer = null
    }

    @Test
    fun stringResourceSameAsGivenString_returnTrue() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        val result = resourceComparer?.isEqual(context, R.string.app_name, "My Application")

        assertThat(result).isTrue()
    }

    @Test
    fun stringResourceDifferentAsGivenString_returnFalse() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        val result = resourceComparer?.isEqual(context, R.string.app_name, "My App")

        assertThat(result).isFalse()
    }

}