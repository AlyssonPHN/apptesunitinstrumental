package com.example.myapplication.data.local

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    version = 1,
    entities = [ShoppingItem::class]
)
abstract class ShoppingItemDatabase: RoomDatabase() {

    abstract fun shoppingDao(): ShoppingDao

}