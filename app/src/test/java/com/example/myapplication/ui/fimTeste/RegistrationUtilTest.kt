package com.example.myapplication.ui.fimTeste


import com.google.common.truth.Truth.assertThat
import org.junit.Test

class RegistrationUtilTest {

    @Test
    fun `empty username return false`(){
        val result = RegistrationUtil.validadeRegistrationInput(
            username = "",
            password = "",
            confirmedPassword = "123"
        )

        assertThat(result).isFalse()
    }

    @Test
    fun `valid username and correctly repeated password return true`(){
        val result = RegistrationUtil.validadeRegistrationInput(
            username = "Philipp",
            password = "123",
            confirmedPassword = "123"
        )

        assertThat(result).isTrue()
    }

    @Test
    fun `username alredy exist return false`(){
        val result = RegistrationUtil.validadeRegistrationInput(
            username = "Carl",
            password = "123",
            confirmedPassword = "123"
        )

        assertThat(result).isFalse()
    }

    @Test
    fun `empty password return false`(){
        val result = RegistrationUtil.validadeRegistrationInput(
            username = "Carl",
            password = "",
            confirmedPassword = ""
        )

        assertThat(result).isFalse()
    }

    @Test
    fun `password was repeated incorrectly return false`(){
        val result = RegistrationUtil.validadeRegistrationInput(
            username = "Philip",
            password = "1234",
            confirmedPassword = "1567"
        )

        assertThat(result).isFalse()
    }


    @Test
    fun `password contains less than 2 digits return false`(){
        val result = RegistrationUtil.validadeRegistrationInput(
            username = "Carl",
            password = "123456",
            confirmedPassword = "123456"
        )

        assertThat(result).isFalse()
    }

}